% The ; means that a new row follows.
A = [1, 2, 3; 4, 5, 6; 7, 8, 9]

% Initialize a vector
v = [1; 2; 3]

% Get the dimension of the matrix A where m = rows and n = columns
[m,n] = size(A)

% You could also store it this way
dim_A = size(A)

% Get the dimension of the vector v
dim_v = size(v)

% Index into the 2nd row and 3rd column of matrix A
A_23 = A(2, 3)
